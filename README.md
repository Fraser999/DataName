# General

This is an example implementation of the proposal from [MaidSafe RFC 0034]
(https://github.com/maidsafe/rfcs/blob/master/text/0034-immutable-data-name-function/0034-immutable-data-name-function.md).

[![build status](https://gitlab.com/Fraser999/DataName/badges/master/build.svg)](https://gitlab.com/Fraser999/DataName/commits/master)

## Comparison

| Type           | Description                                                                                                                           | Can be Invalid | Contents Hashed        | Needs Custom Encode Functions <sup>1</sup> | Can Derive `Ord`, `PartialOrd` and `Hash` |
|:---------------|:--------------------------------------------------------------------------------------------------------------------------------------|:---------------|:-----------------------|:-------------------------------------------|:------------------------------------------|
| [Existing][0]  |                                                                                                                                       | Yes            | every call to `name()` | No                                         | Yes                                       |
| [`Lazy`][1]    | lazy initialisation of optional `name` member variable on call to `name()` or `validate()`                                            | Yes            | maximum once           | No                                         | No                                        |
| [`Minimal`][2] | `name` member variable initialised on call to `new()` or `decode()`                                                                   | Yes            | exactly once           | No                                         | Yes                                       |
| [`Safe`][3]    | `name` member variable initialised on call to `new()` or `deserialise()`, but also validated against expected name in `deserialise()` | No             | exactly once           | Yes                                        | Yes                                       |

---

## Sample Benchmarks for 1MB chunks

| Type           | Benchmark for `name()`         | Benchmark for Serialisation     | Benchmark for Deserialisation   |
|:---------------|-------------------------------:|--------------------------------:|--------------------------------:|
| [Existing][0]  | 2,487,253 ns/iter (+/- 64,476) | 2,313,759 ns/iter (+/- 41,182)  | 5,479,790 ns/iter (+/- 270,810) |
| [`Lazy`][1]    |   124,710 ns/iter (+/- 3,648)  | 2,339,700 ns/iter (+/- 159,777) | 5,483,177 ns/iter (+/- 183,479) |
| [`Minimal`][2] |         0 ns/iter (+/- 1)      | 2,314,490 ns/iter (+/- 29,752)  | 8,081,331 ns/iter (+/- 183,989) |
| [`Safe`][3]    |         0 ns/iter (+/- 0)      | 3,026,149 ns/iter (+/- 89,004)  | 7,237,680 ns/iter (+/- 108,792) |

[0]: https://gitlab.com/Fraser999/DataName/blob/master/src/existing.rs "Source for existing `ImmutableData` implementation."
[1]: https://gitlab.com/Fraser999/DataName/blob/master/src/lazy.rs "Source for `Lazy` implementation."
[2]: https://gitlab.com/Fraser999/DataName/blob/master/src/minimal.rs "Source for `Minimal` implementation."
[3]: https://gitlab.com/Fraser999/DataName/blob/master/src/safe.rs "Source for `Safe` implementation."
