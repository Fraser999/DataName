#![forbid(bad_style, exceeding_bitshifts, mutable_transmutes, no_mangle_const_items,
          unknown_crate_types, warnings)]
#![deny(deprecated, drop_with_repr_extern, improper_ctypes, missing_docs,
        non_shorthand_field_patterns, overflowing_literals, plugin_as_library,
        private_no_mangle_fns, private_no_mangle_statics, stable_features, unconditional_recursion,
        unknown_lints, unsafe_code, unused, unused_allocation, unused_attributes,
        unused_comparisons, unused_features, unused_parens, while_true)]
#![warn(trivial_casts, trivial_numeric_casts, unused_extern_crates, unused_import_braces,
        unused_qualifications, unused_results)]
#![allow(box_pointers, fat_ptr_transmutes, missing_copy_implementations,
         missing_debug_implementations, variant_size_differences)]

#![cfg_attr(feature="clippy", feature(plugin))]
#![cfg_attr(feature="clippy", plugin(clippy))]
#![cfg_attr(feature="clippy", deny(clippy, clippy_pedantic))]

#![feature(test)]
extern crate data_name;
#[macro_use]
extern crate maidsafe_utilities;
extern crate rand;
extern crate test;

use data_name::{ImmutableData, Lazy, Minimal, Safe};
use maidsafe_utilities::serialisation;
use test::Bencher;

#[bench]
fn existing_name(bencher: &mut Bencher) {
    let data = rand::random::<ImmutableData>();
    bencher.iter(|| {
        let _ = data.name();
    });
}

#[bench]
fn lazy_name(bencher: &mut Bencher) {
    let data = rand::random::<Lazy>();
    bencher.iter(|| {
        let _ = data.name();
    });
}

#[bench]
fn minimal_name(bencher: &mut Bencher) {
    let data = rand::random::<Minimal>();
    bencher.iter(|| {
        let _ = data.name();
    });
}

#[bench]
fn safe_name(bencher: &mut Bencher) {
    let data = rand::random::<Safe>();
    bencher.iter(|| {
        let _ = data.name();
    });
}

#[bench]
fn existing_serialisation(bencher: &mut Bencher) {
    let data = rand::random::<ImmutableData>();
    bencher.iter(|| {
        let _ = unwrap_result!(serialisation::serialise(&data));
    });
}

#[bench]
fn lazy_serialisation(bencher: &mut Bencher) {
    let data = rand::random::<Lazy>();
    bencher.iter(|| {
        let _ = unwrap_result!(serialisation::serialise(&data));
    });
}

#[bench]
fn minimal_serialisation(bencher: &mut Bencher) {
    let data = rand::random::<Minimal>();
    bencher.iter(|| {
        let _ = unwrap_result!(serialisation::serialise(&data));
    });
}

#[bench]
fn safe_serialisation(bencher: &mut Bencher) {
    let data = rand::random::<Safe>();
    bencher.iter(|| {
        let _ = unwrap_result!(data.serialise());
    });
}

#[bench]
fn existing_deserialisation(bencher: &mut Bencher) {
    let data = rand::random::<ImmutableData>();
    let serialised_data = unwrap_result!(serialisation::serialise(&data));
    bencher.iter(|| {
        let _ = unwrap_result!(serialisation::deserialise::<ImmutableData>(&serialised_data));
    });
}

#[bench]
fn lazy_deserialisation(bencher: &mut Bencher) {
    let data = rand::random::<Lazy>();
    let serialised_data = unwrap_result!(serialisation::serialise(&data));
    bencher.iter(|| {
        let _ = unwrap_result!(serialisation::deserialise::<Lazy>(&serialised_data));
    });
}

#[bench]
fn minimal_deserialisation(bencher: &mut Bencher) {
    let data = rand::random::<Minimal>();
    let serialised_data = unwrap_result!(serialisation::serialise(&data));
    bencher.iter(|| {
        let _ = unwrap_result!(serialisation::deserialise::<Minimal>(&serialised_data));
    });
}

#[bench]
fn safe_deserialisation(bencher: &mut Bencher) {
    let data = rand::random::<Safe>();
    let name = data.name();
    let serialised_data = unwrap_result!(data.serialise());
    bencher.iter(|| {
        let _ = unwrap_result!(Safe::deserialise(&serialised_data, name));
    });
}
