//! Existing implementation of `ImmutableData`.

use std::fmt::{self, Debug, Formatter};

use rand::{Rand, Rng};
use sodiumoxide::crypto::hash::sha512;
use xor_name::XorName;

#[derive(Hash, Clone, Eq, PartialEq, Ord, PartialOrd, RustcEncodable, RustcDecodable)]
pub struct ImmutableData {
    value: Vec<u8>,
}

impl ImmutableData {
    pub fn new(value: Vec<u8>) -> ImmutableData {
        ImmutableData { value: value }
    }

    pub fn value(&self) -> &Vec<u8> {
        &self.value
    }

    pub fn name(&self) -> XorName {
        XorName(sha512::hash(&self.value).0)
    }
}

impl Debug for ImmutableData {
    fn fmt(&self, formatter: &mut Formatter) -> fmt::Result {
        write!(formatter, "ImmutableData({:?})", self.name())
    }
}

impl Rand for ImmutableData {
    fn rand<R: Rng>(rng: &mut R) -> ImmutableData {
        let value = rng.gen_iter().take(1024 * 1024).collect();
        ImmutableData::new(value)
    }
}



#[cfg(test)]
mod tests {
    use super::*;
    use rand;
    use maidsafe_utilities::serialisation::{deserialise, serialise};

    #[test]
    fn serialise_and_deserialise() {
        let data = rand::random::<ImmutableData>();
        println!("{:?}", data);
        let serialised = unwrap_result!(serialise(&data));
        let parsed = unwrap_result!(deserialise::<ImmutableData>(&serialised));
        assert_eq!(data.name(), parsed.name());
        assert!(data.value() == parsed.value());
    }
}
