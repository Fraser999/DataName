//! Implementation using lazy initialisation of `name` member variable.

use std::cell::{Cell, RefCell, Ref};
use std::fmt::{self, Debug, Formatter};

use rand::{Rand, Rng};
use rustc_serialize::{Decodable, Decoder, Encodable, Encoder};
use sodiumoxide::crypto::hash::sha512;
use xor_name::{XorName, XOR_NAME_LEN};

#[derive(Clone, Eq, PartialEq)]
pub struct Lazy {
    name: RefCell<XorName>,
    name_initialised: Cell<bool>,
    value: Vec<u8>,
}

impl Lazy {
    pub fn new(value: Vec<u8>) -> Lazy {
        Lazy {
            name: RefCell::new(XorName::new([0; XOR_NAME_LEN])),
            name_initialised: Cell::new(false),
            value: value,
        }
    }

    pub fn value(&self) -> &Vec<u8> {
        &self.value
    }

    pub fn name(&self) -> Ref<XorName> {
        if !self.name_initialised.get() {
            *self.name.borrow_mut() = XorName(sha512::hash(&self.value).0);
            self.name_initialised.set(true);
        }
        self.name.borrow()
    }

    pub fn validate(&mut self, expected_name: &XorName) -> bool {
        *self.name() == *expected_name
    }
}

impl Encodable for Lazy {
    fn encode<E: Encoder>(&self, encoder: &mut E) -> Result<(), E::Error> {
        self.value.encode(encoder)
    }
}

impl Decodable for Lazy {
    fn decode<D: Decoder>(decoder: &mut D) -> Result<Lazy, D::Error> {
        let value: Vec<u8> = try!(Decodable::decode(decoder));
        Ok(Lazy {
            name: RefCell::new(XorName::new([0; XOR_NAME_LEN])),
            name_initialised: Cell::new(false),
            value: value,
        })
    }
}

impl Debug for Lazy {
    fn fmt(&self, formatter: &mut Formatter) -> fmt::Result {
        write!(formatter, "Lazy({:?})", self.name())
    }
}

impl Rand for Lazy {
    fn rand<R: Rng>(rng: &mut R) -> Lazy {
        let value = rng.gen_iter().take(1024 * 1024).collect();
        Lazy::new(value)
    }
}



#[cfg(test)]
mod tests {
    use super::*;
    use rand;
    use maidsafe_utilities::serialisation::{deserialise, serialise};

    #[test]
    fn serialise_and_deserialise() {
        let data = rand::random::<Lazy>();
        println!("{:?}", data);
        let _ = data.name();
        println!("{:?}", data);
        let serialised = unwrap_result!(serialise(&data));
        let parsed = unwrap_result!(deserialise::<Lazy>(&serialised));
        assert_eq!(*data.name(), *parsed.name());
        assert!(data.value() == parsed.value());
    }
}
