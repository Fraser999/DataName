//! Implementation which hashes the value exactly once, but doesn't validate the resulting name.

use std::fmt::{self, Debug, Formatter};

use rand::{Rand, Rng};
use rustc_serialize::{Decodable, Decoder, Encodable, Encoder};
use sodiumoxide::crypto::hash::sha512;
use xor_name::XorName;

#[derive(Hash, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub struct Minimal {
    name: XorName,
    value: Vec<u8>,
}

impl Minimal {
    pub fn new(value: Vec<u8>) -> Minimal {
        Minimal {
            name: XorName(sha512::hash(&value).0),
            value: value,
        }
    }

    pub fn value(&self) -> &Vec<u8> {
        &self.value
    }

    pub fn name(&self) -> &XorName {
        &self.name
    }

    pub fn validate(&self, expected_name: &XorName) -> bool {
        self.name == *expected_name
    }
}

impl Encodable for Minimal {
    fn encode<E: Encoder>(&self, encoder: &mut E) -> Result<(), E::Error> {
        self.value.encode(encoder)
    }
}

impl Decodable for Minimal {
    fn decode<D: Decoder>(decoder: &mut D) -> Result<Minimal, D::Error> {
        let value: Vec<u8> = try!(Decodable::decode(decoder));
        let name = XorName(sha512::hash(&value).0);
        Ok(Minimal {
            name: name,
            value: value,
        })
    }
}

impl Debug for Minimal {
    fn fmt(&self, formatter: &mut Formatter) -> fmt::Result {
        write!(formatter, "Minimal({:?})", self.name)
    }
}

impl Rand for Minimal {
    fn rand<R: Rng>(rng: &mut R) -> Minimal {
        let value = rng.gen_iter().take(1024 * 1024).collect();
        Minimal::new(value)
    }
}



#[cfg(test)]
mod tests {
    use super::*;
    use rand;
    use maidsafe_utilities::serialisation::{deserialise, serialise};

    #[test]
    fn serialise_and_deserialise() {
        let data = rand::random::<Minimal>();
        println!("{:?}", data);
        let serialised = unwrap_result!(serialise(&data));
        let parsed = unwrap_result!(deserialise::<Minimal>(&serialised));
        assert_eq!(data.name(), parsed.name());
        assert!(data.value() == parsed.value());
    }
}
