//! Implementation which hashes the value exactly once, and validates the resulting name when
//! parsing.

use std::fmt::{self, Debug, Formatter};

use maidsafe_utilities::serialisation::{self, SerialisationError};
use rand::{Rand, Rng};
use sodiumoxide::crypto::hash::sha512;
use xor_name::XorName;

#[derive(Debug)]
pub enum DataError {
    /// Failure to serialise or deserialise
    Encoding(SerialisationError),
    /// Invalid contents
    Validation,
}

impl From<SerialisationError> for DataError {
    fn from(error: SerialisationError) -> DataError {
        DataError::Encoding(error)
    }
}

#[derive(Hash, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub struct Safe {
    name: XorName,
    value: Vec<u8>,
}

impl Safe {
    pub fn new(value: Vec<u8>) -> Safe {
        Safe {
            name: XorName(sha512::hash(&value).0),
            value: value,
        }
    }

    pub fn value(&self) -> &Vec<u8> {
        &self.value
    }

    pub fn name(&self) -> &XorName {
        &self.name
    }

    pub fn serialise(&self) -> Result<Vec<u8>, DataError> {
        Ok(try!(serialisation::serialise(&self.value)))
    }

    pub fn deserialise(serialised_safe: &[u8], expected_name: &XorName) -> Result<Safe, DataError> {
        let value = try!(serialisation::deserialise::<Vec<u8>>(serialised_safe));
        let name = XorName(sha512::hash(&value).0);
        if name == *expected_name {
            Ok(Safe {
                name: name,
                value: value,
            })
        } else {
            Err(DataError::Validation)
        }
    }
}

impl Debug for Safe {
    fn fmt(&self, formatter: &mut Formatter) -> fmt::Result {
        write!(formatter, "Safe({:?})", self.name)
    }
}

impl Rand for Safe {
    fn rand<R: Rng>(rng: &mut R) -> Safe {
        let value = rng.gen_iter().take(1024 * 1024).collect();
        Safe::new(value)
    }
}



#[cfg(test)]
mod tests {
    use super::*;
    use rand;

    #[test]
    fn serialise_and_deserialise() {
        let data = rand::random::<Safe>();
        let name = data.name();
        println!("{:?}", data);
        let serialised = unwrap_result!(data.serialise());
        let parsed = unwrap_result!(Safe::deserialise(&serialised, name));
        assert_eq!(data.name(), parsed.name());
        assert!(data.value() == parsed.value());
        let bad_name = rand::random();
        assert!(Safe::deserialise(&serialised, &bad_name).is_err());
    }
}
